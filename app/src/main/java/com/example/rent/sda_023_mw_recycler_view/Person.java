package com.example.rent.sda_023_mw_recycler_view;

/**
 * Created by RENT on 2017-05-11.
 */

public class Person {

    private String name;
    private String surname;
    private String address;
    private String telephoneNumber;
    private String creditCardNumber;
    private String pesel;

    public Person(String name, String surname, String address, String telephoneNumber, String creditCardNumber, String pesel) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.telephoneNumber = telephoneNumber;
        this.creditCardNumber = creditCardNumber;
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public String getPesel() {
        return pesel;
    }


}
