package com.example.rent.sda_023_mw_recycler_view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RENT on 2017-05-11.
 */

public class ViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.text_view_name)
    protected TextView name;

    @BindView(R.id.text_view_surname)
    protected TextView surname;

    @BindView(R.id.text_view_adres)
    protected TextView adres;

    @BindView(R.id.text_view_telephone)
    protected TextView telephone;

    @BindView(R.id.text_view_ccn)
    protected TextView ccn;

    @BindView(R.id.text_view_pesel)
    protected TextView pesel;



    public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
