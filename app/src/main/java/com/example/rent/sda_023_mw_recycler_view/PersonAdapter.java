package com.example.rent.sda_023_mw_recycler_view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-05-11.
 */

public class PersonAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Person> personList = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    Person person = personList.get(position);
        holder.name.setText(person.getName());
        holder.surname.setText(person.getSurname());
        holder.adres.setText(person.getAddress());
        holder.telephone.setText(person.getTelephoneNumber());
        holder.ccn.setText(person.getCreditCardNumber());
        holder.pesel.setText(person.getPesel());

    }

    @Override
    public int getItemCount() {
        return personList.size();
    }

    public void add(){

        Person person1 = new Person("a", "b", "c", "d", "e", "f");
        Person person2 = new Person("a", "b", "c", "d", "e", "f");
        Person person3 = new Person("a", "b", "c", "d", "e", "f");
        Person person4 = new Person("a", "b", "c", "d", "e", "f");
        Person person5 = new Person("a", "b", "c", "d", "e", "f");
        Person person6 = new Person("a", "b", "c", "d", "e", "f");
        personList.add(person1);
        personList.add(person2);
        personList.add(person3);
        personList.add(person4);
        personList.add(person5);
        personList.add(person6);

    }
}
